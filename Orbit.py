﻿# -*- coding: utf-8 -*-
import tkinter as tk
import tkinter.messagebox as mbox
import os
import sys
import math
import pygame.mixer
import threading
import queue
#from pyfiglet import Figlet

P = os.path.abspath(os.path.dirname(__file__)) + "\\"

state = False
objnum = 0
deg = 0
rate = 1
max = 100
centerlist = (max * 2)*[0] #ドラッグ可能図形の中心座標のリスト
obj_ids = [] #ドラッグ可能図形のIDのリスト
r = [] #円周の半径
circlenum = 0 #円周の数
circle_ids = [] #円周のIDのリスト
soundid = 0 #重なり判定の出た図形のID

class Orbit():
    """メインクラス：各種処理"""
    def __init__(self, master):
        self.master = master
        #メニューバー生成
        menubar = tk.Menu(self.master)
        self.master.configure(menu=menubar)
        sample = tk.Menu(menubar, tearoff=False)
        menubar.add_cascade(label='Samples', menu=sample)
        self.bass = tk.BooleanVar()
        self.hat = tk.BooleanVar()
        self.snare = tk.BooleanVar()
        self.tom = tk.BooleanVar()
        self.cymbal = tk.BooleanVar()
        self.bass.set(True)
        self.hat.set(False)
        self.snare.set(False)
        self.tom.set(False)
        self.cymbal.set(False)
        sample.add_checkbutton(label='bass', variable=self.bass, command=self.checkbass)
        sample.add_checkbutton(label='hat', variable=self.hat, command=self.checkhat)
        sample.add_checkbutton(label='snare', variable=self.snare, command=self.checksnare)
        sample.add_checkbutton(label='tom', variable=self.tom, command=self.checktom)
        sample.add_checkbutton(label='cymbal', variable=self.cymbal, command=self.checkcymbal)
        rate = tk.Menu(menubar, tearoff=False)
        menubar.add_cascade(label='Rates', menu=rate)
        self.r1 = tk.BooleanVar()
        self.r2 = tk.BooleanVar()
        self.r3 = tk.BooleanVar()
        self.r1.set(True)
        self.r2.set(False)
        self.r3.set(False)
        rate.add_checkbutton(label='1', variable=self.r1, command=self.setr1)
        rate.add_checkbutton(label='2', variable=self.r2, command=self.setr2)
        rate.add_checkbutton(label='3', variable=self.r3, command=self.setr3)

        #キャンバス及び図形の生成
        Orbit.c0 = tk.Canvas(self.master, width=300, height=280, bg='#434447')
        Orbit.c0.grid(row=0, column=1, rowspan=5)

        #画像開く
        self.resetimage = tk.PhotoImage(file=P+'Picts\\reset.gif')
        self.stopimage = tk.PhotoImage(file=P+'Picts\\stop.gif')
        self.playimage = tk.PhotoImage(file=P+'Picts\\play.gif')
        self.bassimage = tk.PhotoImage(file=P+'Picts\\bass.gif')
        self.hatimage = tk.PhotoImage(file=P+'Picts\\hat.gif')
        self.snareimage = tk.PhotoImage(file=P+'Picts\\snare.gif')
        self.tomimage = tk.PhotoImage(file=P+'Picts\\tom.gif')
        self.cymbalimage = tk.PhotoImage(file=P+'Picts\\cymbal.gif')
        self.orbitimage = tk.PhotoImage(file=P+'Picts\\orbit.gif')

        #ボタン生成
        b1 = tk.Button(root, image=self.playimage, command=self.play, state=tk.DISABLED)
        b1.grid(row=0, column=0)
        b2 = tk.Button(root, image=self.resetimage, command=self.init_orbit, state=tk.DISABLED)
        b2.grid(row=1, column=0)
        b3 = tk.Button(root, image=self.bassimage, command=self.makebass)
        b3.grid(row=2, column=0)
        b4 = tk.Button(root, image=self.orbitimage, command=self.ask_radius)
        b4.grid(row=3, column=0)
        b5 = tk.Button(root, text="Exit", command=sys.exit)
        b5.grid(row=4, column=0)

        #initialize
        pygame.mixer.init(frequency=44100, buffer=2048)
        ShowLoop().start()

    #オービットの初期化
    def init_orbit(self):
        if state == True:
            self.stop()
        global deg
        for i in range(circlenum):
            Orbit.c0.coords(circle_ids[i], 140, 140-r[i]-10, 160, 140-r[i]+10)
        deg = 0
        b1 = tk.Button(root, image=self.playimage, command=self.play)
        b1.grid(row=0, column=0)

    #オービット一時停止
    def stop(self):
        global state
        state = False
        b1 = tk.Button(root, image=self.playimage, command=self.play)
        b1.grid(row=0, column=0)

    #オービット再生
    def play(self):
        global state
        if state == False:
            state = True
        b1 = tk.Button(root, image=self.stopimage, command=self.stop)
        b1.grid(row=0, column=0)

    #ドラッグ可能オブジェクト生成
    def makebass(self):
        global objnum, centerlist
        obj_ids.append(Orbit.c0.create_oval(10, 10, 30, 30, fill='black', outline='white', tags='bass'))
        Orbit.c0.tag_bind(obj_ids[objnum], '<Button1-Motion>', self.move)
        Orbit.c0.tag_bind(obj_ids[objnum], '<Button-3>', self.remove)
        centerlist[objnum*2] = 20
        centerlist[objnum*2+1] = 20 #後で修正　関連すべてappendで動的にできるかも？
        objnum += 1
    def makehat(self):
        global objnum
        obj_ids.append(Orbit.c0.create_oval(10, 10, 30, 30, fill='yellow', outline='white', tags='hat'))
        Orbit.c0.tag_bind(obj_ids[objnum], '<Button1-Motion>', self.move)
        Orbit.c0.tag_bind(obj_ids[objnum], '<Button-3>', self.remove)
        centerlist[objnum*2] = 20
        centerlist[objnum*2+1] = 20
        objnum += 1
    def makesnare(self):
        global objnum
        obj_ids.append(Orbit.c0.create_oval(10, 10, 30, 30 ,fill='red', outline='white', tags='snare'))
        Orbit.c0.tag_bind(obj_ids[objnum], '<Button1-Motion>', self.move)
        Orbit.c0.tag_bind(obj_ids[objnum], '<Button-3>', self.remove)
        centerlist[objnum*2] = 20
        centerlist[objnum*2+1] = 20
        objnum += 1
    def maketom(self):
        global objnum
        obj_ids.append(Orbit.c0.create_oval(10, 10, 30, 30, fill='blue', outline='white', tags='tom'))
        Orbit.c0.tag_bind(obj_ids[objnum], '<Button1-Motion>', self.move)
        Orbit.c0.tag_bind(obj_ids[objnum], '<Button-3>', self.remove)
        centerlist[objnum*2] = 20
        centerlist[objnum*2+1] = 20
        objnum += 1
    def makecymbal(self):
        global objnum
        obj_ids.append(Orbit.c0.create_oval(10, 10, 30, 30, fill='#ed9d25', outline='white', tags='cymbal'))
        Orbit.c0.tag_bind(obj_ids[objnum], '<Button1-Motion>', self.move)
        Orbit.c0.tag_bind(obj_ids[objnum], '<Button-3>', self.remove)
        centerlist[objnum*2] = 20
        centerlist[objnum*2+1] = 20
        objnum += 1

    #カーソルと共に移動(make用)
    def move(self, event):
        global centerlist, obj_ids
        x = event.x
        y = event.y
        if x > 0 and x < 300 and y > 0 and y < 280:
            Orbit.c0.coords('current', x - 10,y - 10,x + 10,y + 10)
            recid = Orbit.c0.find_withtag('current')
            id = obj_ids.index(recid[0])
            centerlist[id*2] = x
            centerlist[id*2+1] = y

    #図形削除インスタンス(make用)
    def remove(self, event):
        global objnum
        Orbit.c0.delete('current')
        #objnum -= 1 今の仕様では「通算で」20個までしか作れない(要centerlistの動的管理))

    #半径入力インスタンス
    def ask_radius(self):
        b4 = tk.Button(root, image=self.orbitimage, command = self.ask_radius, state=tk.DISABLED)
        b4.grid(row = 3, column = 0)
        if circlenum < 4:
            self.sub = tk.Toplevel(self.master)
            self.sub.title("radius")
            self.sub.iconbitmap(P+"Picts\\orbit.ico")
            x = self.master.winfo_rootx()
            y = self.master.winfo_rooty()
            geometry = "+%d+%d" % (x+60,y+60)
            self.sub.geometry(geometry)
            self.sub.protocol('WM_DELETE_WINDOW', self.whendestroy)
            scale = tk.Scale(self.sub, from_=30, to=120, orient=tk.HORIZONTAL, length=200)
            scale.grid(row=0, column=0, columnspan=2)
            sb1 = tk.Button(self.sub, text='Make Orbit', command=lambda:self.place_circle(scale.get()))
            sb2 = tk.Button(self.sub, text='Cancel', command=self.whendestroy)
            sb1.grid(row=1, column=0)
            sb2.grid(row=1, column=1)
        else:
            mbox.showerror(title="Error", message=u"オービットは4つまでです")

    #軌道削除インスタンス(place_circle用)
    def removeO(self, event):
        global circlenum
        ball_id = Orbit.c0.find_withtag("current")[0] + 1
        Orbit.c0.delete("current")
        Orbit.c0.delete(ball_id)
        circlenum -= 1
        r.pop(circle_ids.index(ball_id))
        circle_ids.remove(ball_id) #circle_idsにはコメット(ball)のidを追加しているため
        if circlenum < 1:
            b1 = tk.Button(root, image=self.playimage, command=self.play, state=tk.DISABLED)
            b2 = tk.Button(root, image=self.resetimage, command=self.init_orbit, state=tk.DISABLED)
            b1.grid(row=0, column=0)
            b2.grid(row=1, column=0)

    #円周配置インスタンス(ask_radius用)
    def place_circle(self, ret):
        global first, r, circlenum, circle_ids
        circlenum += 1
        if circlenum == 1:
            b1 = tk.Button(root, image=self.playimage, command=self.play)
            b2 = tk.Button(root, image=self.resetimage, command=self.init_orbit)
            b1.grid(row=0, column=0)
            b2.grid(row=1, column=0)
        ccl = Orbit.c0.create_oval(150-ret, 140-ret, 150+ret, 140+ret, width = 2, outline='white')
        Orbit.c0.tag_bind(ccl, '<Button-3>', self.removeO)
        ball = Orbit.c0.create_oval(140, 140-ret-10, 160, 140-ret+10, fill='white', outline='white')
        circle_ids.append(ball)
        Orbit.c0.tag_lower(ball)
        Orbit.c0.tag_lower(ccl)
        r.append(ret)
        b4 = tk.Button(root, image=self.orbitimage, command=self.ask_radius)
        b4.grid(row=3, column=0)
        self.sub.withdraw()

    #サブウインドウが閉じられた時ボタンを再アクティベート
    def whendestroy(self):
        b4 = tk.Button(root, image=self.orbitimage, command=self.ask_radius)
        b4.grid(row=3, column=0)
        self.sub.destroy()

    #楽器ボタン再配置(menubar用)
    def checkbass(self):
        self.hat.set(False)
        self.snare.set(False)
        self.tom.set(False)
        self.cymbal.set(False)
        b3 = tk.Button(root, image=self.bassimage, command=self.makebass)
        b3.grid(row=2, column=0)
    def checkhat(self):
        self.bass.set(False)
        self.snare.set(False)
        self.tom.set(False)
        self.cymbal.set(False)
        b3 = tk.Button(root, image=self.hatimage, command=self.makehat)
        b3.grid(row=2, column=0)
    def checksnare(self):
        self.bass.set(False)
        self.hat.set(False)
        self.tom.set(False)
        self.cymbal.set(False)
        b3 = tk.Button(root, image=self.snareimage, command=self.makesnare)
        b3.grid(row=2, column=0)
    def checktom(self):
        self.bass.set(False)
        self.hat.set(False)
        self.snare.set(False)
        self.cymbal.set(False)
        b3 = tk.Button(root, image=self.tomimage, command=self.maketom)
        b3.grid(row=2, column=0)
    def checkcymbal(self):
        self.bass.set(False)
        self.hat.set(False)
        self.snare.set(False)
        self.tom.set(False)
        b3 = tk.Button(root, image=self.cymbalimage, command=self.makecymbal)
        b3.grid(row=2, column=0)

    #オービットの速度変更(menubar用)
    def setr1(self):
        global rate
        self.r2.set(False)
        self.r3.set(False)
        rate = 1
    def setr2(self):
        global rate
        self.r1.set(False)
        self.r3.set(False)
        rate = 2
    def setr3(self):
        global rate
        self.r1.set(False)
        self.r2.set(False)
        rate = 3

class Showorbit(Orbit):
    """サブクラス：オービットの描画関連"""
    #オービット描画位置指定
    def draw_orbit(self):
        global deg, circle_ids
        for i in range(circlenum):
            x = 150 + r[i] * math.sin(math.radians(deg))
            y = 140 - r[i] * math.cos(math.radians(deg))
            self.c0.coords(circle_ids[i], x - 10, y - 10, x + 10, y + 10)
        deg += 3.6 / rate
        #deg = round(deg, 1)

    #オービットの繰り返し描画
    def show_orbit(self):
        if state == True:
            self.draw_orbit()
            self.check_overlap()
        root.after(10, self.show_orbit)

    #オービットと図形の重なり判定
    sound_arr = [[False for i in range(4)] for j in range(max)] #円周の限界数:4
    prev_arr = [[False for i in range(4)] for j in range(max)]
    def check_overlap(self):
        global obj_ids, soundid, circle_ids, tmp, cnt
        for j in range(circlenum):
            mli = Orbit.c0.coords(circle_ids[j])
            for i in range(objnum):
                tx = centerlist[i*2]
                ty = centerlist[i*2+1]
                if (tx > mli[0] and  tx < mli[2]) and (ty > mli[1] and ty < mli[3]):
                    self.sound_arr[i][j] = True
                    self.c0.coords(obj_ids[i], tx-12, ty-12, tx+12, ty+12) #接触時若干大きくなる
                elif self.prev_arr[i][j] is True:
                    self.c0.coords(obj_ids[i], tx-10, ty-10, tx+10, ty+10) #元に戻す
                    self.sound_arr[i][j] = False
                if self.prev_arr[i][j] == False and self.sound_arr[i][j] == True:
                    soundid = obj_ids[i]
                    self.queue = queue.Queue()
                    Sound(self.queue).start()
                self.prev_arr[i][j] = self.sound_arr[i][j]

class ShowLoop(threading.Thread, Showorbit):
    """サブクラス(スレッド)：オービットの円周運動"""
    def run(self):
        self.show_orbit()

class Sound(threading.Thread):
    """サブクラス(スレッド)：wavの再生"""
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue
        self.bass = pygame.mixer.Sound(P+"Sounds\\bass.wav")
        self.hat = pygame.mixer.Sound(P+"Sounds\\hat.wav")
        self.snare = pygame.mixer.Sound(P+"Sounds\\snare.wav")
        self.tom = pygame.mixer.Sound(P+"Sounds\\tom.wav")
        self.cymbal = pygame.mixer.Sound(P+"Sounds\\cymbal.wav")

    def run(self):
        if soundid in Orbit.c0.find_withtag('bass'):
            self.bass.play()
        elif soundid in Orbit.c0.find_withtag('hat'):
            self.hat.play()
        elif soundid in Orbit.c0.find_withtag('snare'):
            self.snare.play()
        elif soundid in Orbit.c0.find_withtag('tom'):
            self.tom.play()
        elif soundid in Orbit.c0.find_withtag('cymbal'):
            self.cymbal.play()
        self.queue.put("finished")


# ------------------------------------------
if __name__ == '__main__':
    #f = Figlet(font="big")
    #msg = f.renderText(u"Orbit")
    #print(msg)
    print("Orbit version 0.1")
    print(u"created by 高砂　再配布を禁止します　音声には魔王魂様のものを使用しています\n")
    print(u"---あそびかた---\nOrbitは「軌道(オービット)」を用いてかんたんにループサウンドを作れるアプリです。")
    print(u"左にあるボタンのうち、一番上は再生ボタンです。オービットを1つ以上作ることで再生できるようになり、再生中は一時停止ボタンになります。")
    print(u"二番目は停止ボタンです。オービットを1つ以上作ることで再生中に停止できるようになります。(コメットは初期位置に戻ります)")
    print(u"三番目は「プラネット(仮)」という、音の出るオブジェクトを生成するボタンです。上のバーの「Samples」を変えることで、プラネットも変わります。")
    print(u"四番目はオービットを生成するボタンです。押すと半径を決めるためのバーが出てくるので、適当に決めてください。")
    print(u"五番目はExitボタンで、押すと終了できます。")
    print(u"オービットとプラネットは、右クリックすることで消すことができます。")
    print(u"再生ボタンを押すことで、「コメット(仮)」(オービット上の白いオブジェクト)が回転します。回転の速度は上のバーの「Rates」で変えることができます。")
    print(u"左側の三番目のボタンを押して、「プラネット」を生成したら、ドラッグして「オービット」上のどこかに動かしてみてください。")
    print(u"「プラネット」と「コメット」が重なり合うと、音が生まれます。\n")
    print(u"いろいろなプラネットを配置して、自由なサウンドを作り出してみてください！")
    root = tk.Tk()
    root.title("Orbit")
    root.resizable(0, 0)
    root.iconbitmap(P+"Picts\\orbit.ico")
    main_ui = Orbit(root)

    root.mainloop()
